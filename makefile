# Makefile for building compa.
# Assumes that env variables been set, to set tool paths and credentials.
# (For local development, source env.mac)

# Set the version number for compa.
export Version := 1.0

# Set the version of CompB that is required.
export CompBVersion := 1.0

all: proto jar unittest image comptest tag push

.PHONY: all

.ONESHELL

# Compile gRPC spec into gRPC+ProtoBuf code. Note that protoc must be installed,
# and the gRPC module, obtainable from
# http://central.maven.org/maven2/io/grpc/protoc-gen-grpc-java/
# must be renamed to "protoc-gen-grpc-java", and must be in the PATH.
# Ref: https://stackoverflow.com/questions/53976614/protoc-command-not-generating-all-base-classes-java
proto:
	mkdir -p src/main/generated
	protoc \
		--java_out=src/main/java \
		--grpc-java_out=src/main/java \
		src/main/proto/Merger.proto

# Package compa into a JAR file, and push to local maven repository.
jar:
	$(MVN) install

# Run unit level tests.
unittest:
	junit....

# Build Docker image of compa and its dependencies.
image:
	$(MVN) dependency:resolve
	$(MVN) dependency:copy-dependencies
	docker build --tag pdd-example-compa --file Dockerfile target/dependencies

# Run component level tests.
comptest:
	docker run ....pdd-example-compa
	....karate
	docker stop ....pdd-example-compa

# Tag the image to indicate that it has passed unit and comp level tests,
# but don't tag it yet as potentially deployable, since it has not yet been used
# in integration tests. Also tag it with the name of the story that it implements.
tag:
	docker tag ....unit test status
	docker tag ....comp test status
	docker tag ....story name

# Push the image to Dockerhub (only do this if all tests pass)
push:
	docker push ....
