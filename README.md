# pdd-example-compa

Combine two lists, but do not allow duplicates.
If an item exists in both lists, the earlier occurrence is chosen.

Design:
https://drive.google.com/open?id=1B9ZQ8Nn-4JMaqOR3dkf3tTDHS_yeepHciVqmu-qZrbs
