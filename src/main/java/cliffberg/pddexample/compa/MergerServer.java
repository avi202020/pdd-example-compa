package cliffberg.pddexample.compa;

import io.grpc.Server;

public class MergerServer {

	Server server;
	int port;

	/** Create a server using serverBuilder as a base and features as data. */
	public MergerServer(int port, Collection<Feature> features) {
		ServerBuilder<?> serverBuilder = ServerBuilder.forPort(port);
		this.port = port;
		this.server = serverBuilder.addService(new MergerService()).build();
	}

	public void start() throws IOException {
		this.server.start();
		System.out.println("Server started, listening on " + port);
	}

	public void stop() throws IOException {
		if (this.server != null) {
			this.server.shutdown();
			System.out.println("Server stopped");
		}
	}
}
