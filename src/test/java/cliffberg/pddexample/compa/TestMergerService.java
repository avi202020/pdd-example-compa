package cliffberg.pddexample.compa;

import cliffberg.pddexample.compb.ID;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.List;
import java.util.LinkedList;

public class TestMergerService {
	@Test
	public void testDoMerge() {
		List<ID> primaryList = new LinkedList<ID>();
		primaryList.add(new ID("abc"));
		primaryList.add(new ID("def"));

		List<ID> secondaryList = new LinkedList<ID>();
		secondaryList.add(new ID("abc"));
		secondaryList.add(new ID("ghi"));

		List<ID> expectedResult = new LinkedList<ID>();
		expectedResult.add(new ID("abc"));
		expectedResult.add(new ID("def"));
		expectedResult.add(new ID("ghi"));

		List<ID> mergedList = MergerService.doMerge(primaryList, secondaryList);

		assertEquals(mergedList, expectedResult);
	}
}
